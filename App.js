import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableHighlight,
  PermissionsAndroid,
  ActivityIndicator,
} from 'react-native';
import CallDetectorManager from 'react-native-call-detection';
import {apikey} from './keys';

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      start: false,
      incoming: false,
      number: null,
      numberInfo: null,
      loading: false,
      error: null,
      permissionsGranted: false,
    };
  }

  componentDidMount() {
    this.askPermission();
  }

  askPermission = async () => {
    try {
      const permissionResult = await PermissionsAndroid.requestMultiple(
        [
          PermissionsAndroid.PERMISSIONS.READ_CALL_LOG,
          PermissionsAndroid.PERMISSIONS.READ_PHONE_STATE,
        ],
        {
          // For older Androids (isn't visible on newer ones)
          title: 'Permission request',
          message: 'Please give the app permission to track your calls',
          buttonNeutral: 'Ask Me Later',
          buttonNegative: 'Cancel',
          buttonPositive: 'OK',
        },
      );

      if (
        Object.values(permissionResult).every(result => result === 'granted')
      ) {
        console.log('success');
      } else {
        console.log('at least partially denied');
      }
    } catch (err) {
      console.warn(err);
    }
  };

  startListenerTapped = () => {
    this.setState({start: true});
    this.callDetector = new CallDetectorManager(
      (event, number) => {
        // For Android event will be either "Offhook",
        // "Disconnected", "Incoming" or "Missed"
        if (event === 'Disconnected') {
          this.setState({
            incoming: false,
            number: null,
            numberInfo: null,
            error: null,
            loading: false,
          });
        } else if (event === 'Incoming') {
          this.setState({incoming: true, number});
          this.getNumberInfo(number);
        } else if (event === 'Offhook') {
          //Device call state: Off-hook.
          // At least one call exists that is dialing,
          // active, or on hold,
          // and no calls are ringing or waiting.
          // This clause will only be executed for Android
          this.setState({incoming: true, number});
        } else if (event === 'Missed') {
          // Call was missed or declined
          // This clause will only be executed for Android
          this.setState({
            incoming: false,
            number: null,
            error: null,
            loading: false,
            //numberInfo: null,
          });
        }
      },
      true, // true if you want to read the phone number of the incoming call [ANDROID], otherwise false
      () => {},
      // callback if your permission got denied [ANDROID] [only if
      //you want to read incoming number] default: console.error
      {
        title: 'Phone State Permission',
        message:
          'This app needs access to your phone state in order to react and/or to adapt to incoming calls.',
      }, // a custom permission request message to explain to your
      //user, why you need the permission [recommended] - this
      // is the default one
    );
  };

  stopListenerTapped = () => {
    this.setState({
      start: false,
      incoming: false,
      number: null,
      numberInfo: null,
      error: null,
    });
    this.callDetector && this.callDetector.dispose();
  };

  /** Only works if you have the key for the Vainu API. For security reasons, the key is not public and will not be included in the repo */
  getNumberInfo = async number => {
    try {
      this.setState({loading: true});
      const response = await fetch(
        `https://api.vainu.io/api/v1/prospects/filter/?country=FI&phone=` +
          number,
        {
          method: 'GET',
          headers: {'API-key': apikey},
        },
      );
      const info = await response.json();
      // parses JSON response into native JavaScript objects
      if (info && info[0] && this.state.incoming) {
        this.setState({
          numberInfo: info[0],
          loading: false,
        });
      } else {
        this.setState({
          numberInfo: {},
          loading: false,
        });
      }
    } catch (error) {
      this.setState({
        loading: false,
        error: this.state.incoming ? error : null,
      });
    }
  };

  render() {
    const {numberInfo, incoming, number, loading, error} = this.state;
    let result;

    if (numberInfo && numberInfo.industry_code === '62010') {
      // IT industry code
      result = (
        <Text style={{fontSize: 40, textAlign: 'center'}}>
          Potential employer!
        </Text>
      );
    } else if (numberInfo && numberInfo.industry_code === '82200') {
      result = (
        <Text style={{fontSize: 40, textAlign: 'center'}}>Telemarketer</Text>
      );
    } else {
      result = (
        <Text style={{fontSize: 40, textAlign: 'center'}}>
          No information, sorry.{' '}
        </Text>
      );
    }

    return (
      <View style={styles.container}>
        <Text style={styles.text}>Switch on the caller id?</Text>
        <TouchableHighlight
          onPress={
            this.state.start
              ? this.stopListenerTapped
              : this.startListenerTapped
          }>
          <View
            style={{
              width: 200,
              height: 200,
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: this.state.start ? 'greenyellow' : 'indianred',
            }}>
            <Text style={styles.text}>
              {this.state.start ? `Is on` : `Is off`}{' '}
            </Text>
          </View>
        </TouchableHighlight>
        {incoming && <Text style={{fontSize: 20}}>Incoming call {number}</Text>}
        {loading && (
          <View>
            <Text style={{fontSize: 20}}>Loading information</Text>
            <ActivityIndicator size="small" color="green" />
          </View>
        )}
        {numberInfo && result}
        {error && <Text>An error occurred</Text>}
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    backgroundColor: 'honeydew',
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  text: {
    padding: 20,
    fontSize: 20,
  },
});

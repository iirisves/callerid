Start the project with
npm run android

Made in React Native II workshop hosted by RND Works. Thank you to RND Works and their wonderful teacher Laura Järvinen!
Link to their tutorial here: https://medium.com/rnd-works/tagged/react-native
Their own repo for this project can be found here: https://github.com/jarvisenlaura/RND-tutoriaali-soittajantunnistin/
